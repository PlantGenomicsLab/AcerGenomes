#!/bin/bash
#SBATCH --job-name=sickle
#SBATCH -N 1
#SBATCH -n 4
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o sickle_%j.out
#SBATCH -e sickle_%j.err

sickle pe -f L005_R1.fastq.gz -r L005_R2.fastq.gz -t sanger -o trimmed_L005_R1.fastq -p trimmed_L005_R2.fastq -s trimmed_singles_L005.fastq -q 30 -l 100
sickle pe -f L006_R1.fastq.gz -r L006_R2.fastq.gz -t sanger -o trimmed_L006_R1.fastq -p trimmed_L006_R2.fastq -s trimmed_singles_L006.fastq -q 30 -l 100
