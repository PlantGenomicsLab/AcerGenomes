#!/bin/bash
#SBATCH --job-name=bwa
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=400G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o bwa_%j.out
#SBATCH -e bwa_%j.err

module load bwa/0.7.17
module load samtools/1.7
module load samblaster/0.1.24

bwa index ../../assemblies/falcon/purgehap/cns_p_ctg_repeats.fasta
bwa mem -5SP ../../assemblies/falcon/purgehap/cns_p_ctg_repeats.fasta acne_hic_r1.fastq acne_hic_r2.fastq | samblaster | samtools view -S -h -b -F 2316 > acne_falcon_hic_qc_samblaster.bam
