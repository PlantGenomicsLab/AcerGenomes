#!/bin/bash
#SBATCH --job-name=hicqc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o hicqc_%j.out
#SBATCH -e hicqc_%j.err

hostname
echo "\nStart time:"
date

conda activate hic_qc

export PATH="$PATH:/home/CAM/smcevoy/miniconda3/pkgs:/home/CAM/smcevoy/miniconda3/pkgs/pysam-0.15.2-py36hb06f55c_2"

python hic_qc.py -b ~/acne/reads/hic_reads/acne_falcon_hic_qc_samblaster.bam -r -n 5000000 -o acne_5M_qc

echo "\nEnd time:"
date

