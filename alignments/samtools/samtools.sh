#!/bin/bash
#SBATCH --job-name=samtools
#SBATCH -o samtools-%j.output
#SBATCH -e samtools-%j.error
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=60G

module load samtools/1.7

# convert each file from .sam to .bam and sort individually
for f in ../hisat/*.sam
do
	f2=${f#"../hisat/"}
        samtools view -b -@ 16 $f | samtools sort -o sorted_"${f2%.sam}.bam" -@ 16
done

