#!/bin/bash
#SBATCH --job-name=valNeBam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o valNeBam_%j.out
#SBATCH -e valNeBam_%j.err

module load samtools/1.7

samtools flagstat sorted_acsa_genome.bam

# index for idxstats
samtools index sorted_acsa_genome.bam

# number of reads
samtools idxstats sorted_acsa_genome.bam | awk '{s+=$3+$4} END {print s}'

# number of mapped reads
samtools idxstats sorted_acsa_genome.bam | awk '{s+=$3} END {print s}'
