#!/bin/bash
#SBATCH --job-name=minimap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=475G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o minimap_%j.out
#SBATCH -e minimap_%j.err

module load samtools/1.3.1

# using local_software installation of minimap2
minimap2 -ax map-pb -t 30 ../../genome/scaffolds.fasta ../../reads/pb_raw_fasta/acsa_pb.fasta.gz > acsa_pb_nomask.sam
samtools view -hF 256 acsa_pb_nomask.sam \
    | samtools sort -@ 30 -m 475G -o acsa_pb_nomask.bam
samtools flagstat acsa_pb_nomask.bam
