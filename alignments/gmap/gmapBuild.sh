#!/bin/bash
#SBATCH --job-name=gmapB
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH -o gmapB_%j.out
#SBATCH -e gmapB_%j.err

#build gmap index
module load gmap/2017-03-17

indexdir=/labs/Wegrzyn/AcerGenomes/acsa_re/analysis/alignments/gmap/index
genomepath=/labs/Wegrzyn/AcerGenomes/acsa_re/genome/acsa_genome.fasta

gmap_build -D $indexdir -d acsa_genome $genomepath 

