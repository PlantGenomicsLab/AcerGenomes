#!/bin/bash
#SBATCH --job-name=gmapSaNe
#SBATCH -n 9
#SBATCH -N 1
#SBATCH -o gmapSaNe_%j.o
#SBATCH -e gmapSaNe_%j.e
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=180G

module load gmap/2017-03-17

idx="/labs/Wegrzyn/AcerGenomes/acsa_re/analysis/alignments/gmap/index/acsa_genome"
prefix="acsa"
fasta="/labs/Wegrzyn/AcerGenomes/acsa_re/transcriptome/transcripts/cluster/centroids_acsa.fasta"

# larger genomes will require the gmapl command instead of just 'gmap'
 
gmap -D "$idx" -d "$prefix"_genome -f gff3_gene "$fasta" --nthreads=16 --min-intronlength=9 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > "$prefix"_gmap.gff3 2> "$prefix"_gmap.error



