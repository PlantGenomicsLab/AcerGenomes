#!/bin/bash
#SBATCH --job-name=hisatAcne
#SBATCH -o hisatAcne-%j.out
#SBATCH -e hisatAcne-%j.err
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=6
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=280G

module load hisat2/2.1.0

org="/labs/Wegrzyn/AcerGenomes/acsa_re"
trimmeddir="$org/transcriptome/reads/trimmed_reads"
genomeidx="$org/analysis/alignments/hisat/acsa_genome"
f1=$trimmeddir"/trimmed_SACC_1.fastq"
f2=$trimmeddir"/trimmed_SACC_2.fastq"

# I ought to have used the --dta flag - as this as needed
hisat2 -q -x $genomeidx -1 $f1 -2 $f2 -S acsa_genome.sam
