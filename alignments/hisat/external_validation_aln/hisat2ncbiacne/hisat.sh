#!/bin/bash
#SBATCH --job-name=hisatNeNcbiNe
#SBATCH -o hisatNeNcbiNe-%j.out
#SBATCH -e hisatNeNcbiNe-%j.err
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=9
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G

module load hisat2/2.1.0

orgdir="/UCHC/LABS/Wegrzyn/AcerGenomes/acsa"
trimmeddir="/UCHC/LABS/Wegrzyn/AcerGenomes/acne/transcriptome/raw_reads_transcriptome"
genomeidx=$orgdir"/analysis/hisat2/acsa_genome"
f1=$trimmeddir"/sickle/trimmed_ERR2040475_1.fastq"
f2=$trimmeddir"/sickle/trimmed_ERR2040475_2.fastq"
hisat2 -q -x $genomeidx -1 $f1 -2 $f2 -S acsa_genome_ncbi_ne.sam
