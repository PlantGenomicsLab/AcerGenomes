#!/bin/bash
#SBATCH --job-name=hisatAcsa
#SBATCH -o hisatAcsa-%j.out
#SBATCH -e hisatAcsa-%j.err
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=150G

module load hisat2/2.1.0

orgdir="/UCHC/LABS/Wegrzyn/AcerGenomes/acsa"
trimmeddir=$orgdir"/transcriptome/raw_reads_transcriptome"
genomeidx=$orgdir"/analysis/hisat2/acsa_genome"
f1=$trimmeddir"/sickle/trimmed_SRR1778905_1.fastq"
f2=$trimmeddir"/sickle/trimmed_SRR1778905_2.fastq"

hisat2 -q -x $genomeidx -1 $f1 -2 $f2 -S acsa_genome_ncbi.sam
