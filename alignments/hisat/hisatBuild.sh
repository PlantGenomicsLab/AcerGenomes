#!/bin/bash
#SBATCH --job-name=hisatB
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=19
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=300G
#SBATCH -o hisatB_%j.out
#SBATCH -e hisatB_%j.err

module load hisat2/2.1.0

basedir="/labs/Wegrzyn/AcerGenomes/acsa_re/genome"
genome="acsa_genome.fasta"

hisat2-build -f "$basedir/$genome"  acsa_genome

