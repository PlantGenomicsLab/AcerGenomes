#!/bin/bash
#SBATCH --job-name=valNeBam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o valNeBam_%j.out
#SBATCH -e valNeBam_%j.err

module load samtools/1.7

samtools flagstat acsa_genome.sam

