#!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --mem=100GB
#SBATCH --job-name=gth
#SBATCH -o gth-%j.o
#SBATCH -e gth-%j.e
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general

module load genomethreader/1.7.1
module load genometools/1.5.10

org="/labs/Wegrzyn/AcerGenomes/acsa_re"
pep="$org/transcriptome/transcripts/clusterfaa/centroids_acsa.faa"
genome="$org/genome/acsa_genome.fasta"
out="acsa.gth.gff3"

gt seqtransform -addstopaminos $pep > tmp

gth -genomic $genome -protein tmp -gff3out -startcodon -gcmincoverage 80 -finalstopcodon -introncutout -dpminexonlen 20 -skipalignmentout -o $out -force -gcmaxgapwidth 1000000

rm tmp
