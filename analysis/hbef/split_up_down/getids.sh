#!/bin/bash

for fname in all_al_co-results-up.csv all_al_co-results-down.csv fall_al_co-results-up.csv fall_al_co-results-down.csv sp_al_co-results-up.csv sp_al_co-results-down.csv
do
  cut -f2 -d ',' "$fname" > "${fname%.*}.txt"
done
