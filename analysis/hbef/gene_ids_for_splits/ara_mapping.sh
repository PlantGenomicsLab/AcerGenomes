#!/bin/bash

#for fname in *.txt
for fname in all_al_co-results-up.txt all_al_co-results-down.txt fall_al_co-results-up.txt fall_al_co-results-down.txt sp_al_co-results-up.txt sp_al_co-results-down.txt
do
grep -wFf "$fname" ../blastp_acsa_structural_annotation_final_arabidopsis_at11.out > ../arabidopsis_mapping/ara_${fname%.*}.tsv
done
