#!/bin/bash

for fname in *.txt
do
grep -wFf "$fname" ../acsa_only_blastp_proteome_vit.out > ../vitis_mapping/vit_${fname%.*}.tsv
done
