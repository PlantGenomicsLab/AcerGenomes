emapper_version	2.0
original_file	fall_al_ca-results-down.fasta
job_input	/data/shared/emapper_jobs/user_data/MM_ikrn1vau/query_seqs.fa
job_output	query_seqs.fa
job_output_dir	/data/shared/emapper_jobs/user_data/MM_ikrn1vau
email	susan.mcevoy@uconn.edu
job_name	MM_ikrn1vau
job_path	/data/shared/emapper_jobs/user_data/MM_ikrn1vau
nseqs	17
nsites	14714
seq_type	aa
job_cpus	6
database	none
go_evidence	non-electronic
search_mode	diamond
orthology_type	all
seed_evalue	0.001
seed_score	60
query_cov	20
subject_cov	0
tax_scope	auto
emapper_flags	
date_created	07/20/20
cmdline	python2 "$EMAPPERPATH"/emapper.py --cpu "6"        -i "/data/shared/emapper_jobs/user_data/MM_ikrn1vau/query_seqs.fa" --output "query_seqs.fa"        --output_dir "/data/shared/emapper_jobs/user_data/MM_ikrn1vau"        -m "diamond" -d "none"        --tax_scope "auto"        --go_evidence "non-electronic"        --target_orthologs "all"        --seed_ortholog_evalue 0.001        --seed_ortholog_score 60        --query-cover 20        --subject-cover 0        --override                --temp_dir "/data/shared/emapper_jobs/user_data/MM_ikrn1vau"
