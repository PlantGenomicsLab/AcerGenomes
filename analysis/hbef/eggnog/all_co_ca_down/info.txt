emapper_version	2.0
original_file	all_co_ca-results-down.fasta
job_input	/data/shared/emapper_jobs/user_data/MM_fsjzl_yb/query_seqs.fa
job_output	query_seqs.fa
job_output_dir	/data/shared/emapper_jobs/user_data/MM_fsjzl_yb
email	susan.mcevoy@uconn.edu
job_name	MM_fsjzl_yb
job_path	/data/shared/emapper_jobs/user_data/MM_fsjzl_yb
nseqs	2
nsites	1121
seq_type	aa
job_cpus	6
database	none
go_evidence	non-electronic
search_mode	diamond
orthology_type	all
seed_evalue	0.001
seed_score	60
query_cov	20
subject_cov	0
tax_scope	auto
emapper_flags	
date_created	07/20/20
cmdline	python2 "$EMAPPERPATH"/emapper.py --cpu "6"        -i "/data/shared/emapper_jobs/user_data/MM_fsjzl_yb/query_seqs.fa" --output "query_seqs.fa"        --output_dir "/data/shared/emapper_jobs/user_data/MM_fsjzl_yb"        -m "diamond" -d "none"        --tax_scope "auto"        --go_evidence "non-electronic"        --target_orthologs "all"        --seed_ortholog_evalue 0.001        --seed_ortholog_score 60        --query-cover 20        --subject-cover 0        --override                --temp_dir "/data/shared/emapper_jobs/user_data/MM_fsjzl_yb"
