filenames = ["acne_contracted_vit", "acne_expanded_vit","acne_missing_vit","acne_novel_vit","acsa_contracted_vit","acsa_expanded_vit","acsa_missing_vit","acsa_novel_vit","contracted_vit","expanded_vit","missing_vit","novel_vit"]

for filename in filenames:
    with open("%s.tsv" % filename, "r") as f:
        lines = f.read().splitlines()

    out = open("%s_uniq.tsv" % filename, "w")

    highest = ["","",0,0,0,0,0,0,0,0,0,0,0,""]

    for line in lines:
    	#print line
        l = line.split("\t")
        if highest[0] == "":
            highest = l
        elif l[0] == highest[0] and float(l[2]) > float(highest[2]):
        #elif l[0] == highest[0]:
            highest = l 
        elif l[0] != highest[0]:
            out.write("\t".join(highest))
            out.write("\n")
            highest = l
    out.write("\t".join(highest))
    out.close()




