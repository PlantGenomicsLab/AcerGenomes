### Load up those output files from orthofinder and entap

with open("Orthogroups.GeneCount.tsv", "r") as genecounts:
    gcounts = genecounts.read().splitlines()

with open("orthogroup_longest.txt", "r") as orthogroup_longest:
  orthogroups = orthogroup_longest.read().splitlines()

#### calculate mean and stdev for number in each orthogroup for the angiosperm trees
#### also convert the number in each orthogroup for the neuropterans to int from string objects
import statistics 
import re
from operator import itemgetter
from ast import literal_eval

acsa = [ ]
ac_mean = []
ac_stdev = []
non_acsa_trees_mean = []
non_acsa_trees_stdev = []

for og in gcounts:
    column = og.split("\t")
    if column[0] != "":
        angiosperm_trees = [int(column[6]), int(column[7]), int(column[8]), int(column[9]), int(column[10]), int(column[12]), int(column[13]), int(column[16]), int(column[17]), int(column[18]), int(column[19]), int(column[20]), int(column[21])]
        angiosperm_other = [int(column[4]), int(column[5]), int(column[14]), int(column[15]), int(column[22])]
        non_acsa_trees = angiosperm_trees
        non_acsa_trees.append(int(column[1]))
        non_acsa_trees.append(int(column[3]))
        acsa.append(int(column[2]))
        non_acsa_trees_mean.append(statistics.mean(non_acsa_trees))
        non_acsa_trees_stdev.append(statistics.stdev(non_acsa_trees))
        
### Compare mean and stdev to numbers in each orthogroup for to get expanded and contracted list
###expanded if are 2 stdev above the mean, greater than or equal to 2, and not completely absent
###contracted if are 2 stdev below the mean, not equal to 0, and with an average of at least 2

sa_exfile = open ("comparisons/acsa_expanded.txt" , "w")
sa_confile = open ("comparisons/acsa_contracted.txt" , "w")
sa_novfile = open ("comparisons/acsa_novel.txt" , "w")
sa_misfile = open ("comparisons/acsa_missing.txt" , "w")

count = 0
for (mean, stdev, sa, og) in zip(non_acsa_trees_mean, non_acsa_trees_stdev, acsa, orthogroups):     
    if sa > (mean + 2 * stdev) and sa >=2 and mean!=0:
        sa_exfile.writelines(og)
        sa_exfile.write("\n")
    elif sa < (mean - 2 * stdev) and sa != 0 and mean >= 2:
        sa_confile.writelines(og)
        sa_confile.write("\n")
    elif sa == 0 and mean >= 1:
        sa_misfile.writelines(og)
        sa_misfile.write("\n")
    elif sa != 0 and mean == 0:
        sa_novfile.writelines(og)
        sa_novfile.write("\n")

sa_exfile.close()        
sa_confile.close()    
sa_novfile.close()   
sa_misfile.close()

