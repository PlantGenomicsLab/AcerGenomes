### Load up those output files from orthofinder and entap

with open("Orthogroups.GeneCount.tsv", "r") as genecounts:
    gcounts = genecounts.read().splitlines()

with open("orthogroup_longest.txt", "r") as orthogroup_longest:
  orthogroups = orthogroup_longest.read().splitlines()

#### calculate mean and stdev for number in each orthogroup for the angiosperm trees
#### also convert the number in each orthogroup for the neuropterans to int from string objects
import statistics 
import re
from operator import itemgetter
from ast import literal_eval

angiosperm_trees_mean = [ ]
angiosperm_trees_stdev = [ ]
gymnosperm = [ ]
ac_mean = []
ac_stdev = []
non_acsa_trees_mean = []
non_acsa_trees_stdev = []

for og in gcounts:
    column = og.split("\t")
    if column[0] != "":
        angiosperm_trees = [int(column[6]), int(column[7]), int(column[8]), int(column[9]), int(column[10]), int(column[12]), int(column[13]), int(column[16]), int(column[17]), int(column[18]), int(column[19]), int(column[20]), int(column[21])]
        angiosperm_other = [int(column[4]), int(column[5]), int(column[14]), int(column[15]), int(column[22])]
        gymnosperm.append(int(column[11]))
        angiosperm_trees_mean.append(statistics.mean(angiosperm_trees))
        angiosperm_trees_stdev.append(statistics.stdev(angiosperm_trees))
        acers = [int(column[1]), int(column[2]), int(column[3])]
        ac_mean.append(statistics.mean(acers))
        ac_stdev.append(statistics.stdev(acers))

### Compare mean and stdev to numbers in each orthogroup for to get expanded and contracted list
###expanded if are 2 stdev above the mean, greater than or equal to 2, and not completely absent
###contracted if are 2 stdev below the mean, not equal to 0, and with an average of at least 2

exfile = open ("comparisons/expanded.txt" , "w")
confile = open ("comparisons/contracted.txt" , "w")
novfile = open ("comparisons/novel.txt" , "w")
misfile = open ("comparisons/missing.txt" , "w")

count = 0
for (mean, stdev, ac, og) in zip(angiosperm_trees_mean, angiosperm_trees_stdev, ac_mean, orthogroups): 
    if ac > (mean + 2 * stdev) and ac >= 2 and mean!=0:
        exfile.writelines(og)
        exfile.write("\n")
    elif ac < (mean - 2 * stdev) and ac != 0 and mean >= 2:
        confile.writelines(og)
        confile.write("\n")
    elif ac == 0 and mean >= 1:
        misfile.writelines(og)
        misfile.write("\n")
    elif ac != 0 and mean == 0:
        novfile.writelines(og)
        novfile.write("\n")

exfile.close()        
confile.close()    
novfile.close()   
misfile.close()
