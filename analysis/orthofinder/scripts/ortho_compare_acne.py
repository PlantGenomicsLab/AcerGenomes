### Load up those output files from orthofinder and entap

with open("Orthogroups.GeneCount.tsv", "r") as genecounts:
    gcounts = genecounts.read().splitlines()

with open("orthogroup_longest.txt", "r") as orthogroup_longest:
  orthogroups = orthogroup_longest.read().splitlines()

#### calculate mean and stdev for number in each orthogroup for the angiosperm trees
#### also convert the number in each orthogroup for the neuropterans to int from string objects
import statistics 
import re
from operator import itemgetter
from ast import literal_eval

acne = [ ]
ac_mean = []
ac_stdev = []
non_acne_trees_mean = []
non_acne_trees_stdev = []

for og in gcounts:
    column = og.split("\t")
    if column[0] != "":
        angiosperm_trees = [int(column[6]), int(column[7]), int(column[8]), int(column[9]), int(column[10]), int(column[12]), int(column[13]), int(column[16]), int(column[17]), int(column[18]), int(column[19]), int(column[20]), int(column[21])]
        angiosperm_other = [int(column[4]), int(column[5]), int(column[14]), int(column[15]), int(column[22])]
        non_acne_trees = angiosperm_trees
        non_acne_trees.append(int(column[2]))
        non_acne_trees.append(int(column[3]))
        acne.append(int(column[1]))
        non_acne_trees_mean.append(statistics.mean(non_acne_trees))
        non_acne_trees_stdev.append(statistics.stdev(non_acne_trees))
        
### Compare mean and stdev to numbers in each orthogroup for to get expanded and contracted list
###expanded if are 2 stdev above the mean, greater than or equal to 2, and not completely absent
###contracted if are 2 stdev below the mean, not equal to 0, and with an average of at least 2

ne_exfile = open ("comparisons/acne_expanded.txt" , "w")
ne_confile = open ("comparisons/acne_contracted.txt" , "w")
ne_novfile = open ("comparisons/acne_novel.txt" , "w")
ne_misfile = open ("comparisons/acne_missing.txt" , "w")

count = 0
for (mean, stdev, ne, og) in zip(non_acne_trees_mean, non_acne_trees_stdev, acne, orthogroups):     
    if ne > (mean + 2 * stdev) and ne >=2 and mean!=0:
        ne_exfile.writelines(og)
        ne_exfile.write("\n")
    elif ne < (mean - 2 * stdev) and ne != 0 and mean >= 2:
        ne_confile.writelines(og)
        ne_confile.write("\n")
    elif ne == 0 and mean >= 1:
        ne_misfile.writelines(og)
        ne_misfile.write("\n")
    elif ne != 0 and mean == 0:
        ne_novfile.writelines(og)
        ne_novfile.write("\n")

ne_exfile.close()        
ne_confile.close()    
ne_novfile.close()   
ne_misfile.close()

