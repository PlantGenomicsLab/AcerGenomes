emapper_version	2.0
original_file	acne_probabilities_seqs_down.faa
job_input	/data/shared/emapper_jobs/user_data/MM_j234up23/query_seqs.fa
job_output	query_seqs.fa
job_output_dir	/data/shared/emapper_jobs/user_data/MM_j234up23
email	susanlmcevoy@gmail.com
job_name	MM_j234up23
job_path	/data/shared/emapper_jobs/user_data/MM_j234up23
nseqs	52
nsites	78106
seq_type	aa
job_cpus	6
database	none
go_evidence	non-electronic
search_mode	diamond
orthology_type	all
seed_evalue	0.001
seed_score	60
query_cov	20
subject_cov	0
tax_scope	auto
emapper_flags	
date_created	06/30/20
cmdline	python2 "$EMAPPERPATH"/emapper.py --cpu "6"        -i "/data/shared/emapper_jobs/user_data/MM_j234up23/query_seqs.fa" --output "query_seqs.fa"        --output_dir "/data/shared/emapper_jobs/user_data/MM_j234up23"        -m "diamond" -d "none"        --tax_scope "auto"        --go_evidence "non-electronic"        --target_orthologs "all"        --seed_ortholog_evalue 0.001        --seed_ortholog_score 60        --query-cover 20        --subject-cover 0        --override                --temp_dir "/data/shared/emapper_jobs/user_data/MM_j234up23"
