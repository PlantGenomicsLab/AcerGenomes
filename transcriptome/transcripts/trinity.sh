#!/bin/bash

#############################
##XANADU SLURM CONFIGURATION#
#############################

#SBATCH --job-name=trntyACER
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH -p himem1,himem2,himem3,himem4,himem5
#SBATCH --mail-type=ALL # other options are ALL, NONE, BEGIN, FAIL
#SBATCH --mail-user=uzay.sezen@uconn.edu
#SBATCH --mem=500G
#
#SBATCH -o trntyACER-%j.out
#SBATCH -e trntyACER-%j.err

#
#

####################
##END CONFIGURATION$
####################

##Log Start time
date
hostname

##Load your modules here

module load trinity/2.2.0

##Your bash commands here


Trinity --seqType fq --max_memory 500G --left /home/CAM/usezen/ForestGEO/Acer/GRIS/trimmed_GRIS_1.fastq --right /home/CAM/usezen/ForestGEO/Acer/GRIS/trimmed_GRIS_2.fastq --min_contig_length 300 --CPU 32 --output /home/CAM/usezen/ForestGEO/Acer/GRIS/trinity.GRIS.out

Trinity --seqType fq --max_memory 500G --left /home/CAM/usezen/ForestGEO/Acer/NEGU/trimmed_NEGU_1.fastq --right /home/CAM/usezen/ForestGEO/Acer/NEGU/trimmed_NEGU_2.fastq --min_contig_length 300 --CPU 32 --output /home/CAM/usezen/ForestGEO/Acer/NEGU/trinity.NEGU.out

Trinity --seqType fq --max_memory 500G --left /home/CAM/usezen/ForestGEO/Acer/SACC/trimmed_SACC_1.fastq --right /home/CAM/usezen/ForestGEO/Acer/SACC/trimmed_SACC_2.fastq --min_contig_length 300 --CPU 32 --output /home/CAM/usezen/ForestGEO/Acer/SACC/trinity.SACC.out

##log ending time

date
