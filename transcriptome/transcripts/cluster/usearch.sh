#!/bin/bash
#SBATCH --job-name=usearchSa
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=150G
#SBATCH -o usearchNe_%j.out
#SBATCH -e usearchNe_%j.err

module load usearch/9.0.2132
usearch --cluster_fast ../acsa_transcriptome.faa --centroids centroids_acsa.faa --uc centroids_acsa.faa.uc --id 0.90
