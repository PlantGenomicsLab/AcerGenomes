#!/bin/bash
#SBATCH --job-name=get_frameselected_rm_contams
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=45G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o get_frameselected_rm_contams_%j.out
#SBATCH -e get_frameselected_rm_contams_%j.err

fr_sel_fnn="orig_uzay/enTAP/frame_selection/SACC.Trinity.fasta.fnn"
fr_sel_faa="orig_uzay/enTAP/frame_selection/SACC.Trinity.fasta.faa"
contam_file="orig_uzay/enTAP/final_annotations_lvl0_contam.tsv"

#cut -f 1 $contam_file > contam_ids.txt
#tail -n +2 contam_ids.txt > contam_ids.tmp && mv contam_ids.tmp contam_ids.txt  
#grep 'TRINITY_'  $fr_sel_fnn | tr -d '>' > fr_sel_ids_fnn.txt
grep 'TRINITY_'  $fr_sel_faa | tr -d '>' > fr_sel_ids_faa.txt
#grep -vFf contam_ids.txt fr_sel_ids_fnn.txt > fr_sel_no_contams_fnn.txt
grep -vFf contam_ids.txt fr_sel_ids_faa.txt > fr_sel_no_contams_faa.txt
#python createFasta.py --fasta $fr_sel_fnn --nameList fr_sel_no_contams_fnn.txt --out acsa_tr_fr_sel_no_contams.fnn
python createFasta.py --fasta $fr_sel_faa --nameList fr_sel_no_contams_faa.txt --out acsa_tr_fr_sel_no_contams.faa

