#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 9
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=45G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o quast_%j.out
#SBATCH -e quast_%j.err

input="acsa_transcriptome.fasta.clustered"
module load quast/5.0.2
python /isg/shared/apps/quast/5.0.2/quast.py "$input" --min-contig 0 -o quast_o -t 9
