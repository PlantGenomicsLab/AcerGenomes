#!/bin/bash
#SBATCH --job-name=renameheader
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=45G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o renameheader_%j.out
#SBATCH -e renameheader_%j.err

#awk '/^>/{print ">acsa_" sprintf("%05d", ++i); next}{ print }' < acsa_tr_fr_sel_no_contams.fnn> acsa_transcriptome.fasta

awk '/^>/{print ">acsa_" sprintf("%05d", ++i); next}{ print }' < acsa_tr_fr_sel_no_contams.faa > acsa_transcriptome.faa
