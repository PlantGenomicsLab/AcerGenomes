#!/bin/bash
#SBATCH --job-name=GRIS
#SBATCH --mail-user=uzay.sezen@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=120G
#SBATCH -o GRIS.out
#SBATCH -e GRIS.err
#SBATCH --partition=general
#SBATCH --qos=general

module load anaconda/4.4.0
module load perl/5.28.0
#export PERL5LIB=/UCHC/PublicShare/szaman/uzay/perl5
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0

#
/UCHC/LABS/Wegrzyn/EnTAP/EnTAP --runP -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /UCHC/LABS/Wegrzyn/Transcriptomics/Araport11/arabidopsis_at11.dmnd -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.88.dmnd -d /isg/shared/databases/Diamond/ntnr/nr_protein.88.dmnd --trim -i /home/CAM/usezen/ForestGEO/Acer/GRIS/GRIS.Trinity.fasta --out-dir /home/CAM/usezen/ForestGEO/Acer/GRIS/enTAP -c bacteria -c archaea -c opisthokonta -c rhizaria -c fungi --threads 16
