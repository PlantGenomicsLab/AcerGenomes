#!/bin/bash
#SBATCH --job-name=filterFasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=35G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o filterFasta_%j.out
#SBATCH -e filterFasta_%j.err

hostname
echo "\nStart time:"
date

grep '>' cluster/centroids_acne.faa | tr -d '>' > faa_ids.txt
python createFasta.py --fasta acne_transcriptome.fasta --nameList faa_ids.txt --out acne_transcriptome.fasta.filtered

echo "\nEnd time:"
date

