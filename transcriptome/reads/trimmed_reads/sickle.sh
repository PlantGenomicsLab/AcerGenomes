#!/bin/bash

#############################
##XANADU SLURM CONFIGURATION#
#############################

#SBATCH --job-name=sickleForestGEO
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH -p general
#SBATCH --mail-type=ALL # other options are ALL, NONE, BEGIN, FAIL
#SBATCH --mail-user=uzay.sezen@uconn.edu
#SBATCH --mem=16G
#
#SBATCH -o sickleForestGEO-%j.out
#SBATCH -e sickleForestGEO-%j.err


#

####################
##END CONFIGURATION$
####################

##Log Start time
date
hostname

##Load your modules here

module load sickle

##Your bash commands here




sickle pe -f /home/CAM/usezen/ForestGEO/Acer/GRIS/GRIS_1.fq -r /home/CAM/usezen/ForestGEO/Acer/GRIS/GRIS_2.fq -t sanger -o /home/CAM/usezen/ForestGEO/Acer/GRIS/trimmed_GRIS_1.fastq -p /home/CAM/usezen/ForestGEO/Acer/GRIS/trimmed_GRIS_2.fastq -s /home/CAM/usezen/ForestGEO/Acer/GRIS/trimmed_singles_GRIS.fastq -q 35 -l 45

sickle pe -f /home/CAM/usezen/ForestGEO/Acer/NEGU/NEGU_1.fq -r /home/CAM/usezen/ForestGEO/Acer/NEGU/NEGU_2.fq -t sanger -o /home/CAM/usezen/ForestGEO/Acer/NEGU/trimmed_NEGU_1.fastq -p /home/CAM/usezen/ForestGEO/Acer/NEGU/trimmed_NEGU_2.fastq -s /home/CAM/usezen/ForestGEO/Acer/NEGU/trimmed_singles_NEGU.fastq -q 35 -l 45

sickle pe -f /home/CAM/usezen/ForestGEO/Acer/SACC/SACC_1.fq -r /home/CAM/usezen/ForestGEO/Acer/SACC/SACC_2.fq -t sanger -o /home/CAM/usezen/ForestGEO/Acer/SACC/trimmed_SACC_1.fastq -p /home/CAM/usezen/ForestGEO/Acer/SACC/trimmed_SACC_2.fastq -s /home/CAM/usezen/ForestGEO/Acer/SACC/trimmed_singles_SACC.fastq -q 35 -l 45

#sickle pe -f /home/CAM/usezen/ForestGEO/Acer/XXXX/XXXX_1.fq -r /home/CAM/usezen/ForestGEO/Acer/XXXX/XXXX_2.fq -t sanger -o /home/CAM/usezen/ForestGEO/Acer/XXXX/trimmed_XXXX_1.fastq -p /home/CAM/usezen/ForestGEO/Acer/XXXX/trimmed_XXXX_2.fastq -s /home/CAM/usezen/ForestGEO/Acer/XXXX/trimmed_singles_XXXX.fastq -q 35 -l 45


##log ending time
date
