#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=sickle
#SBATCH -o sickle_%j.out
#SBATCH -e sickle_%j.err
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=4
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general


### This sickle script is designed for PAIRED end reads and is path dependent!!!
### variable myarr tries to capture all of the accession numbers (eg. SRR3723923_1.fastq & SRR3723923_2.fastq)
### variable uniq_file tries to capture all of the UNIQUE accession numbers (eg. SRR3723923)
### the unique accession numbers are then used to specify the forward (var f1) files and reverse (var f2) files
### the forward and reverse file names are used to run sickle 


#run this script in the directory you would like the output to be

rawreadsdir="../fetchSRA"
module load sickle/1.33

sickle pe -f $rawreadsdir/SRR1778905_1.fastq -r $rawreadsdir/SRR1778905_2.fastq -t sanger -o trimmed_SRR1778905_1.fastq -p trimmed_SRR1778905_2.fastq -s single_trimmed_SRR1778905.fastq -q 30 -l 50
