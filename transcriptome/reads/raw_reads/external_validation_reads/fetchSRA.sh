#!/bin/bash
#SBATCH --job-name=fetchSRA
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH -o fetchSRA_%j.out
#SBATCH -e fetchSRA_%j.err

module load  sratoolkit/2.8.1

FILENAME="input.txt"
count=0

while read LINE
do
    let count++
    fastq-dump --split-files $LINE
    echo "$LINE"

done < $FILENAME
echo -e "\nTotal lines read = $count"
