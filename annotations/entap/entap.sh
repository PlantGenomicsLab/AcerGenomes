#!/bin/bash
#SBATCH --job-name=entNE
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 25
#SBATCH --mem=150G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load anaconda/4.4.0
module load perl/5.28.1
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0

/labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/EnTAP --paths /labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/entap_config.txt --runP -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.92.dmnd -d /labs/Wegrzyn/Transcriptomics/Araport11/arabidopsis_at11.dmnd -i ../acne_structural_annotation.faa --out-dir /home/CAM/smcevoy/acne/analysis/annotation/entap/ --taxon Acer_negundo -c bacteria -c amoeba -c insecta -c fungi --threads 25

