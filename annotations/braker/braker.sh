#!/bin/bash
#SBATCH --job-name=brakNe
#SBATCH -o brakNe-%j.output
#SBATCH -e brakNe-%j.error
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=30
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=250G

module load BRAKER/2.0.5
module unload singularity/3.1.1
module unload augustus/3.3.3
module load augustus/3.2.3
module load bamtools/2.4.1
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config
export TMPDIR=/scratch/smcevoy/acsa/braker/tmp
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.4.1/bin/
export GENEMARK_PATH=/labs/Wegrzyn/local_software/gm_et_linux_64/gmes_petap/
module unload perl/5.28.1
module load perl/5.24.0
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/
export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/
cp ~/local_gm_key_64 ~/.gm_key


org="/labs/Wegrzyn/AcerGenomes/acsa"
bam="$org/analysis/alignments/samtools/sorted_acsa_genome.bam"
genome="$org/genome/acsa_genome.fasta"
species="acsahicbraker"
protein="$org/analysis/alignments/gth/acsa.gth.gff3"

braker.pl --cores 20 --genome="$genome" --species="$species" --bam "$bam" --GENEMARK_PATH=/labs/Wegrzyn/local_software/gm_et_linux_64/gmes_petap/ --prot_aln="$protein" --prg=gth --gth2traingenes --softmasking 1 --gff3
