#!/bin/bash
#SBATCH --job-name=transcriptOverlaps
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o transcriptOverlaps_%j.out
#SBATCH -e transcriptOverlaps_%j.err

hostname
echo "\nStart time:"
date

# find the difference between the two files which is the genes that were filtered out.  Need to use first colum TCONS
cut -f1 u_braker.tracking | grep -vFf - u.tracking > u_filtered.tracking

# get the transcript ids (no gene ids here) and find these annotations in the orig braker file
cut -f5 u_filtered.tracking | sed 's/^.*q1://g' | sed 's/.mrna.*$//g' > u_filtered_transcriptids.txt

# use the transcript ids to find the perfect matches in the braker tracking file
grep -Ff u_filtered_transcriptids.txt ../acnebraker.tracking | grep '\s=\s' > u_filtered_matches.tracking

# get the gene ids and find these annotations in the orig braker annotation
cut -f3 u_filtered_matches.tracking | sed 's/|/\n/g' | sed 's/$/;/g' > u_filtered_matches_geneids.txt
grep -Ff u_filtered_matches_geneids.txt ../../braker_hic/braker/acnehicbraker2/augustus.hints.gff3 > u_filtered_braker_genes.gff3

# used gfacs to check these filtered genes.  monoexonics, comlete only. multiexonic, partial ok
mkdir u_filtered_braker_genes_gfacs_mono
sbatch u_filtered_braker_genes_gfacs_mono.sh

mkdir u_filtered_braker_genes_gfacs_multi
sbatch u_filtered_braker_genes_gfacs_multi.sh

echo "\nEnd time:"
date

