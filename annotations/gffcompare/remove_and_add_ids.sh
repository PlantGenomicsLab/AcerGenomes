#!/bin/bash
#SBATCH --job-name=remove_geneids.txt
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o remove_geneids.txt_%j.out
#SBATCH -e remove_geneids.txt_%j.err

hostname
echo "\nStart time:"
date

### These lines are just to provide one example, you will need to identify which genes or transcripts you want to replace or add and modify the files and lists of ids accordingly


#grep -Ff k/k_transcripts_matching_refmultis_gfacs_multi_complete/k_transcripts_matching_refmultis_gfacs_multi_complete_transcriptids_to_remove.txt acnebraker.tracking | cut -f3 | sed 's/|.*$//g' > k_transcripts_matching_refmultis_gfacs_multi_complete_geneids_to_remove.txt

#grep -Ff k/k_transcripts_matching_refmonos_gfacs_multi_complete/k_transcripts_matching_refmonos_gfacs_multi_complete_transcriptids_to_remove.txt acnebraker.tracking | cut -f3 | sed 's/|.*$//g' > k_transcripts_matching_refmonos_gfacs_multi_complete_geneids_to_remove.txt

#cat k_transcripts_matching_refmultis_gfacs_multi_complete_geneids_to_remove.txt k_transcripts_matching_refmonos_gfacs_multi_complete_geneids_to_remove.txt | sed 's/^/ID=/g' | sed 's/$/;/g' > geneids_to_remove.txt

#grep 'gene' acne_structural_annotation_gene_table.txt | cut -f6 | sed 's/;.*$//g' | sed 's/$/;/g' > geneids.txt

#grep -vFf geneids_to_remove.txt geneids.txt | sed 's/;//g' | sed 's/\..*$//g' > geneids_to_keep.txt

#python filtergFACsGeneTable.py --table acne_structural_annotation_gene_table.txt --tablePath . --idList geneids_to_keep.txt --idPath . --out acne_structural_annotation_gene_table_geneids_removed.txt

#cat acne_structural_annotation_gene_table_geneids_removed.txt k/k_additions_gene_table.txt u/u_additions_gene_table.txt u/u_filtered_transcripts_matching_brakermonos_gfacs_mono/gene_table.txt k/k_transcripts_matching_refmultis_gfacs_multi_complete/gene_table.txt k/k_transcripts_matching_refmonos_gfacs_multi_complete/gene_table.txt > acne_structural_annotation_gene_table_geneids_removed_and_added.txt

echo "\nEnd time:"
date

