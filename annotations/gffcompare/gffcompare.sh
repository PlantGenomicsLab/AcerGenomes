#!/bin/bash
#SBATCH --job-name=gffcompare
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o gffcompare_%j.out
#SBATCH -e gffcompare_%j.err

#module load gffcompare

#change the gmap/gfacs gtf output to get this to work
# :%s/Parent=/transcript_id "/g
# :%s/mrna1/mrna1";/g

/labs/Wegrzyn/local_software/gffcompare-0.11.5/gffcompare -o acne -s /labs/Wegrzyn/AcerGenomes/acne/genome/acne_genome.fasta -r ../acne_structural_annotation.gtf ../../alignments/gmap_fulllength/gfacs_o/out_transcriptid.gtf

/labs/Wegrzyn/local_software/gffcompare-0.11.5/gffcompare -o acnebraker -s /labs/Wegrzyn/AcerGenomes/acne/genome/acne_genome.fasta -r ../braker_hic/braker/acnehicbraker2/augustus.hints.gtf ../../alignments/gmap_fulllength/gfacs_o/out_transcriptid.gtf
