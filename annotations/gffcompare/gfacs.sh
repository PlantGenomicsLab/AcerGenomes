#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 8
#SBATCH -N 1
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=850G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o gfacsk-%j.o
#SBATCH -e gfacsk-%j.e

module load perl

org="/labs/Wegrzyn/AcerGenomes/acne/"
genome="$org/genome/acne_genome.fasta"
alignment="acne_structural_annotation_gene_table_geneids_removed_and_added.txt"
script="/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--get-protein-fasta \
--create-gtf \
--create-gff3 \
--fasta "$genome" \
-O gfacs_o \
"$alignment"

