#!/bin/bash
#SBATCH --job-name=transcriptOverlaps
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o transcriptOverlaps_%j.out
#SBATCH -e transcriptOverlaps_%j.err

hostname
echo "\nStart time:"
date

# get the gene ids and find these annotations in the orig braker file
cut -f3 k.tracking | sed 's/\.t.*$//g' | sed 's/^/ID=/g'  > k_refids.txt
python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath ../../gfacs/gfacs_hic/final_o --idList k_refids.txt --idPath . --out k_ref_gene_table.txt

# used gfacs to check these filtered genes.  monoexonics, comlete only. multiexonic, partial ok
mkdir k_ref_genes_gfacs_mono
sbatch k_ref_genes_gfacs_mono.sh

mkdir k_ref_genes_gfacs_multi
sbatch k_ref_genes_gfacs_multi.sh

echo "\nEnd time:"
date

