#!/bin/bash
#SBATCH --job-name=transcriptOverlaps
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o transcriptOverlaps_%j.out
#SBATCH -e transcriptOverlaps_%j.err

hostname
echo "\nStart time:"
date

# find the difference between the two files which is the genes that were filtered out.  Need to use first colum TCONS
cut -f1 k.tracking | grep -vFf - k_braker.tracking > k_filtered.tracking

# get the gene ids and find these annotations in the orig braker file
cut -f3 k_filtered.tracking | sed 's/|/\n/g' | sed 's/$/;/g' > k_filtered_brakerids.txt
grep -Ff k_filtered_brakerids.txt ../../braker_hic/braker/acnehicbraker2/augustus.hints.gff3 > k_filtered_braker_genes.gff3

# used gfacs to check these filtered genes.  monoexonics, comlete only. multiexonic, partial ok
mkdir k_filtered_braker_genes_gfacs_mono
sbatch k_filtered_braker_genes_gfacs_mono.sh

mkdir k_filtered_braker_genes_gfacs_multi
sbatch k_filtered_braker_genes_gfacs_multi.sh

echo "\nEnd time:"
date

