#!/bin/bash
#SBATCH --job-name=cafe
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 22
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=320G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o cafe_%j.out
#SBATCH -e cafe_%j.err

hostname
echo "\nStart time:"
date

module load python/3.8.1
module load CAFE/5.0b2

cafexp -i large_filtered_gene_families.txt -t newick.txt.ultrametric.tre -p -l 0.013262260654078 -o large_results

echo "\nEnd time:"
date

