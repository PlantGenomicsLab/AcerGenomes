#!/bin/bash
#SBATCH --job-name=ult
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o ult_%j.out
#SBATCH -e ult_%j.err

hostname
echo "\nStart time:"
date

# Cafe rounds - multiply values by 100 or 1000 before running this.

python /isg/shared/apps/OrthoFinder/2.3.7/tools/make_ultrametric.py newick.txt

echo "\nEnd time:"
date

