#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o filter_%j.out
#SBATCH -e filter_%j.err

hostname
echo "\nStart time:"
date

python /isg/shared/apps/CAFE/5.0b2/tutorial/clade_and_size_filter.py -i all_gene_families.txt -o filtered_gene_families.txt -s
echo "\nEnd time:"
date

