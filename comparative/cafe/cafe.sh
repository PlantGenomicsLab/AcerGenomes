#!/bin/bash
#SBATCH --job-name=cafe
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 22
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=320G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o cafe_%j.out
#SBATCH -e cafe_%j.err

hostname
echo "\nStart time:"
date

module load python/3.8.1
module load CAFE/5.0b2

cafexp -i filtered_gene_families.txt -t newick.txt.ultrametric.tre -p -o results1
cafexp -i filtered_gene_families.txt -t newick.txt.ultrametric.tre -p -o results2
cafexp -i filtered_gene_families.txt -t newick.txt.ultrametric.tre -p -o results3

echo "\nEnd time:"
date

