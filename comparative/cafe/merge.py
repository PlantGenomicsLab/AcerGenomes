with open("Orthogroups.GeneCount.tsv") as families:
    lines = families.read().splitlines()
with open("orthogroup_longest.txt") as orthogroups:
    ogs = orthogroups.read().splitlines()
output = open ("all_gene_families.txt", "w")

for (og, line) in zip(ogs, lines):
    o = og.split("\t")
    l = line.split("\t")
    del l[-1]
    o = [o[1]]
    merged = o + l
    output.write("\t".join(merged))
    output.write("\n")
