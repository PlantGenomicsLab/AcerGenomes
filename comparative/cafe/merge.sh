#!/bin/bash
#SBATCH --job-name=merge
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o merge_%j.out
#SBATCH -e merge_%j.err

hostname
echo "\nStart time:"
date

python merge.py

echo "\nEnd time:"
date

