# AcerGenomes
## Computational methods used for maples

<a href="methods.md">Detailed steps and guidance for scripts on the methods page</a>

## Manuscript

<a href="https://www.biorxiv.org/content/10.1101/2021.07.19.452996v3">Strategies of tolerance reflected in two North American maple genomes</a>
## Acer negundo (box elder)

NCBI BioProject: [PRJNA750066](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA750066)  
TreeGenes: https://treegenesdb.org/org/Acer-negundo  
Data: /labs/Wegrzyn/AcerGenomes/acne/  
[Data tracking sheets](https://docs.google.com/spreadsheets/d/1-hve3B4kbYwPFwTt5VkGzsUO3r2Ge2lCnjnYz7_yHn4/edit#gid=82514878)  
Illumina pe 150bp; 208X coverage  
PacBio 141X coverage  
Hi-C 230k pe 75bp NextSeq High Output  
[Size estimation in GenomeScope](http://qb.cshl.edu/genomescope/analysis.php?code=2kFK4gADdd6dGCySk6xS) - 318,847,077bp  

## Acer saccharum (sugar maple)

NCBI BioProject: [PRJNA748028](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA748028)  
TreeGenes: https://treegenesdb.org/org/Acer-saccharum  
Data: /labs/Wegrzyn/AcerGenomes/acsa/  
[Data tracking sheets](https://docs.google.com/spreadsheets/d/1He-8onf_1Hg0WUtV74gvWP5G2qXa0OuBJ9RFA664fX8/edit#gid=82514878)  
Illumina pe 150bp; 168X coverage  
PacBio 103X coverage  
Hi-C 192k pe 75bp NextSeq High Output  
[Genome size estimation in GenomeScope](http://genomescope.org/analysis.php?code=X0xoR0vdPr4FEXVOqtun) - 635,506,204bp  

## Acer griseum (paperbark)

Data: /labs/WegrzynAcerGenomes/acgr/  
[Data tracking sheets](https://docs.google.com/spreadsheets/d/13JYNak0eDez1SnEUcdJKNg5tWFO7BOxNpsJjpN4YyMo/edit#gid=261923967)  
Illumina pe 250 HiSeq 4000 Rapid Run 2x250bp; 170X coverage  
Nanopore GridIon sequencing in process  
[Genome size estimation in GenomeScope](http://qb.cshl.edu/genomescope/analysis.php?code=1qLJ9qtv6C9csmhNeD1y)    

