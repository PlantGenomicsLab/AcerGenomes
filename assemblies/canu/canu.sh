#!/bin/bash
#SBATCH --job-name=canuSa
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 24
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o canuSa_%j.out
#SBATCH -e canuSa_%j.err

module load gnuplot/5.2.2
module load canu/1.6

# example: canu -d run1 -p godzilla genomeSize=1g -nanopore-raw reads/*.fasta.gz

canu -p acsa -d canu_o genomeSize=700m -pacbio-raw /home/CAM/smcevoy/acers/acsa/raw_reads/Acer_saccharum/all_pb_raw_acsa.fastq.gz gnuplotTested=true gridOptions="--partition=general --qos=general  --mem-per-cpu=8032m --cpus-per-task=24" canuIteration=1


