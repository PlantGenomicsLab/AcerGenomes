#!/bin/bash
#SBATCH --job-name=readstats
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o readstats_%j.out
#SBATCH -e readstats_%j.err

module load anaconda/2.4.1
source activate py27

#DBstats 0-rawreads/build/raw_reads.db > raw_read_stats.txt

#DBstats 1-preads_ovl/build/preads.db > raw_preads_stats.txt

#python /labs/Wegrzyn/local_software/pb-assembly/scripts/get_asm_stats.py 2-asm-falcon/p_ctg.fa > assembly_performance_p.txt

#python /labs/Wegrzyn/local_software/pb-assembly/scripts/get_asm_stats.py 3-unzip/all_p_ctg.fa > unzipped_asm_p.txt
#python /labs/Wegrzyn/local_software/pb-assembly/scripts/get_asm_stats.py 3-unzip/all_h_ctg.fa > unzipped_asm_h.txt

python /labs/Wegrzyn/local_software/pb-assembly/scripts/get_asm_stats.py 4-polish/cns-output/cns_p_ctg.fasta > p_polished.txt
python /labs/Wegrzyn/local_software/pb-assembly/scripts/get_asm_stats.py 4-polish/cns-output/cns_h_ctg.fasta > h_polished.txt
