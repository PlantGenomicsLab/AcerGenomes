#!/bin/bash
#SBATCH --job-name=polarstar
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o polarstar_%j.out
#SBATCH -e polarstar_%j.err

snakemake -p -s Snakefile
