#!/bin/bash
#SBATCH --job-name=purgehre
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 25
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=450G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o purgehapre_%j.out
#SBATCH -e purgehapre_%j.err

module load bedtools/2.25.0
module load samtools/1.3.1
module load minimap2/20170804
module load MUMmer/4.0.2
module load perl/5.28.1
module load R/3.5.1

# purge_haplotigs readhist -b ../minimap-nomask/acsa_pb_nomask.bam -g ../../genome/scaffolds.fasta -t 25

# purge_haplotigs  contigcov  -i acsa_pb_nomask.bam.gencov  -l 10  -m 64  -h 165

purge_haplotigs purge -g ../../genome/scaffolds.fasta -c coverage_stats.csv -r acsa_repeats_6col.bed -t 25
