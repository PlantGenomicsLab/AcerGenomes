#!/bin/bash
#SBATCH --job-name=novNe
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=400G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o novoplasty_%j.out
#SBATCH -e novoplasty_%j.err

hostname
echo "\nStart time:"
date

# seed read from here: https://www.ncbi.nlm.nih.gov/nuccore/DQ978417.1?report=fasta
# chloroplast
# perl /labs/Wegrzyn/local_software/NOVOPlasty/NOVOPlasty3.7.1.pl -c config.txt
# mitochondria
# https://www.ncbi.nlm.nih.gov/nuccore/EU701103.1?report=fasta
perl /labs/Wegrzyn/local_software/NOVOPlasty/NOVOPlasty3.7.1.pl -c config_mito.txt
echo "\nEnd time:"
date

