#!/bin/bash
#SBATCH --job-name=quast-f-c
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 9
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o quast-f-c_%j.out
#SBATCH -e quast-f-c_%j.err

input="/UCHC/LABS/Wegrzyn/AcerGenomes/acne/assemblies/flye-corrected/flye_o/scaffolds.fasta"
module load quast
python /isg/shared/apps/quast/4.6/quast.py "$input" -o quast_o -t 9
