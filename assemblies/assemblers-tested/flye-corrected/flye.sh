#!/bin/bash
#SBATCH --job-name=flye-c
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=240G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o flye-c_%j.out
#SBATCH -e flye-c_%j.err

module load flye/2.3.7

flye --asm-coverage 50 --pacbio-corr acsa.correctedReads.fasta.gz --out-dir flye_o --genome-size 400m --threads 20
