#!/bin/bash
#SBATCH --job-name=canuNe
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=240G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o canuNe_%j.out
#SBATCH -e canuNe_%j.err

module load canu/1.6
module load gnuplot/5.2.2

# example: canu -d run1 -p godzilla genomeSize=1g -nanopore-raw reads/*.fasta.gz

canu -p acne -d canu_o ovsMethod=sequential useGrid=false genomeSize=700m -pacbio-corrected /home/CAM/smcevoy/acers/acne/ccs/pbccs.fasta

