#!/bin/bash
#SBATCH --job-name=pbjelly
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=240G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o pbjelly_%j.out
#SBATCH -e pbjelly_%j.err

module load python/2.7.14

#Jelly.py setup AcneProtocol.xml
#Jelly.py mapping AcneProtocol.xml
#Jelly.py support AcneProtocol.xml
#Jelly.py extraction AcneProtocol.xml
#Jelly.py assembly AcneProtocol.xml 
Jelly.py output AcneProtocol.xml
