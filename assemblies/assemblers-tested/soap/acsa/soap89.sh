#!/bin/bash
#SBATCH --job-name=acsa_soap89
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 25
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#from docs: For big genomes like human, about 150 GB memory would be required.
#SBATCH --mem=250G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o acsa_soap89_%j.out
#SBATCH -e acsa_soap89_%j.err

echo `hostname`
module load SOAP-denovo/2.04
SOAPdenovo-127mer all -s /UCHC/LABS/Wegrzyn/AcerGenomes/illumina/acsa/soap/acsa_soap.config -p 25 -K 89 -R -o graph_acsa89 1>assembly89.log 2>assembly89.err
