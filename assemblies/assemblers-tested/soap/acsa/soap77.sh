#!/bin/bash
#SBATCH --job-name=acsa_soap77
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#from docs: For big genomes like human, about 150 GB memory would be required.
#SBATCH --mem=200G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o acsa_soap77_%j.out
#SBATCH -e acsa_soap77_%j.err

echo `hostname`
module load SOAP-denovo/2.04
SOAPdenovo-127mer all -s /UCHC/LABS/Wegrzyn/AcerGenomes/illumina/acsa/soap/acsa_soap.config -p 20 -K 77 -R -o graph_acsa77 1>assembly77.log 2>assembly77.err

