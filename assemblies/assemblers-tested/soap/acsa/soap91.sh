#!/bin/bash
#SBATCH --job-name=acsa_soap91
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 25
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#from docs: For big genomes like human, about 150 GB memory would be required.
#SBATCH --mem=250G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o acsa_soap91_%j.out
#SBATCH -e acsa_soap91_%j.err

echo `hostname`
module load SOAP-denovo/2.04
#SOAPdenovo-127mer all -s /UCHC/LABS/Wegrzyn/AcerGenomes/illumina/acsa/soap/acsa_soap.config -p 25 -K 91 -R -o graph_acsa91 1>assembly91.log 2>assembly91.err
# run was interrupted during the 4th step, so the following command is to re-run that part
SOAPdenovo-127mer scaff -g graph_acsa91 -F 1>scaff.log 2>scaff.err
