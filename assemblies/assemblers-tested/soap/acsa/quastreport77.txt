All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).
Suggestion: assembly graph_acsa77.scafSeq contains continuous fragments of N's of length >= 10 bp. You may consider rerunning QUAST using --scaffolds (-s) option!

Assembly                    graph_acsa77.scafSeq
# contigs (>= 0 bp)         2628068             
# contigs (>= 1000 bp)      160139              
# contigs (>= 5000 bp)      24379               
# contigs (>= 10000 bp)     6874                
# contigs (>= 25000 bp)     536                 
# contigs (>= 50000 bp)     24                  
Total length (>= 0 bp)      984736560           
Total length (>= 1000 bp)   510783480           
Total length (>= 5000 bp)   226624026           
Total length (>= 10000 bp)  107045788           
Total length (>= 25000 bp)  17341347            
Total length (>= 50000 bp)  1442915             
# contigs                   290917              
Largest contig              100357              
Total length                602022145           
GC (%)                      35.10               
N50                         3392                
N75                         1472                
L50                         42561               
L75                         111240              
# N's per 100 kbp           17702.79            
