#!/bin/bash
#SBATCH --job-name=acsa_soap81
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#from docs: For big genomes like human, about 150 GB memory would be required.
#SBATCH --mem=200G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o acsa_soap81_%j.out
#SBATCH -e acsa_soap81_%j.err

echo `hostname`
module load SOAP-denovo/2.04
SOAPdenovo-127mer all -s /UCHC/LABS/Wegrzyn/AcerGenomes/illumina/acsa/soap/acsa_soap.config -p 20 -K 81 -R -o graph_acsa81 1>assembly81.log 2>assembly81.err

