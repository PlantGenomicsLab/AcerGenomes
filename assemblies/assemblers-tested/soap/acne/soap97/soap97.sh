#!/bin/bash
#SBATCH --job-name=acne_soap97
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#from docs: For big genomes like human, about 150 GB memory would be required.
#SBATCH --mem=225G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o acne_soap97_%j.out
#SBATCH -e acne_soap97_%j.err

module load SOAP-denovo/2.04
SOAPdenovo-127mer all -s /UCHC/LABS/Wegrzyn/AcerGenomes/illumina/acne/soap/acne_soap.config -p 20 -K 97 -R -o graph_acne97 1>assembly97.log 2>assembly97.err
