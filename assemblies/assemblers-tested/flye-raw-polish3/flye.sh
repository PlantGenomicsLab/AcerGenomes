#!/bin/bash
#SBATCH --job-name=flyepolish
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=240G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o flyepolish_%j.out
#SBATCH -e flyepolish_%j.err

module load flye/2.3.7

flye --asm-coverage 50 --pacbio-raw /home/CAM/smcevoy/acers/acne/raw_reads/Acer_negundo/all_pb_raw_acne.fastq.gz --out-dir flye_o --genome-size 670m --threads 20 --iterations 3 --resume-from polishing
