#!/bin/bash
#SBATCH --job-name=spades_carefulSa
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=450G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o spades_carefulSa%j.out
#SBATCH -e spades_carefulSa%j.err

/UCHC/LABS/Wegrzyn/local_software/SPAdes-3.12.0-Linux/bin/spades.py -k 61,71,81,91,101 --careful --pe1-1 /home/CAM/smcevoy/acers/illumina/acsa/sickle/trimmed_all_R1.fastq --pe1-2 /home/CAM/smcevoy/acers/illumina/acsa/sickle/trimmed_all_R2.fastq --s1 /home/CAM/smcevoy/acers/illumina/acsa/sickle/trimmed_singles_all.fastq --s2 /home/CAM/smcevoy/acers/acsa/ccs/20180615_C01/ccs.fq.tar.gz --s2 /home/CAM/smcevoy/acers/acsa/ccs/20180615_D01/ccs.fq.tar.gz --s2 /home/CAM/smcevoy/acers/acsa/ccs/20180618_E01/ccs.fq.tar.gz --s2 /home/CAM/smcevoy/acers/acsa/ccs/20180618_F01/ccs.fq.tar.gz --s2 /home/CAM/smcevoy/acers/acsa/ccs/20180618_G01/ccs.fq.tar.gz --s2 /home/CAM/smcevoy/acers/acsa/ccs/20180618_H01/ccs.fq.tar.gz --s2 /home/CAM/smcevoy/acers/acsa/ccs/20180622_D01/ccs.fq.tar.gz -t 30 -m 450 -o spades_output

