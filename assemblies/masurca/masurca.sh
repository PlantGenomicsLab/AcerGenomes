#!/bin/bash
#SBATCH --job-name=masurca
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=800G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load singularity/3.1.1
module load MaSuRCA/3.3.4

#masurca config.txt
./assemble.sh
