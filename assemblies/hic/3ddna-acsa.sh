#!/bin/bash
#SBATCH --job-name=3dacsa5
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 25
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=875G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o 3ddna_%j.out
#SBATCH -e 3ddna_%j.err

hostname
echo "\nStart time:"
date

export PATH="${PATH}:/labs/Wegrzyn/local_software/lastz"
bash /labs/Wegrzyn/local_software/3d-dna/run-asm-pipeline.sh 3 -i 10000 --editor-coarse-resolution 250000 --editor-coarse-region 500000 --editor-repeat-coverage 3 -m diploid /home/CAM/smcevoy/acsa/assemblies/falcon/purgehap/cns_p_ctg_repeats.fasta  ../../juicer/falcon_purgehap/aligned/merged_nodups.txt

#bash /labs/Wegrzyn/local_software/3d-dna/run-asm-pipeline.sh -i 5000 --editor-coarse-resolution 10000  --editor-repeat-coverage 5 --editor-coarse-region 100000 -e /home/CAM/smcevoy/acsa/assemblies/falcon/purgehap/cns_p_ctg_repeats.fasta  ../../juicer/falcon_purgehap/aligned/merged_nodups.txt

#bash /labs/Wegrzyn/local_software/3d-dna/run-asm-pipeline.sh -i 5000 --rounds 5 --editor-coarse-resolution 10000  --editor-repeat-coverage 5 --editor-coarse-region 100000 /home/CAM/smcevoy/acsa/assemblies/falcon/purgehap/cns_p_ctg_repeats.fasta  ../../juicer/falcon_purgehap/aligned/merged_nodups.txt
echo "\nEnd time:"
date

