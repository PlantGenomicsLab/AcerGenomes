#!/bin/bash
#SBATCH --job-name=juicer
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -p general
#SBATCH -q general
###SBATCH --mail-type=END
#SBATCH --mem=40G
###SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o juicer_%j.out
#SBATCH -e juicer_%j.err

module load java-sdk/1.8.0_92
module load bwa/0.7.17

#python /labs/Wegrzyn/local_software/juicer/misc/generate_site_positions.py Sau3AI acsa_falcon_purgehap_editor2 /labs/Wegrzyn/AcerGenomes/acsa/assemblies/hic/3d-dna-pipeline/falcon_purgehap_editor2/cns_p_ctg_repeats.FINAL.fasta

#awk 'BEGIN{OFS="\t"}{print $1, $NF}' acsa_falcon_purgehap_editor2_Sau3AI.txt > acsa_falcon_purgehap_editor2.chrom.sizes

#bwa index /labs/Wegrzyn/AcerGenomes/acsa/assemblies/hic/3d-dna-pipeline/falcon_purgehap_editor2/cns_p_ctg_repeats.FINAL.fasta

bash /labs/Wegrzyn/local_software/juicer/SLURM/scripts/juicer.sh -f -g acsa_falcon_purgehap_editor2 -y acsa_falcon_purgehap_editor2_Sau3AI.txt -z /labs/Wegrzyn/AcerGenomes/acsa/assemblies/hic/3d-dna-pipeline/falcon_purgehap_editor2/cns_p_ctg_repeats.FINAL.fasta -p acsa_falcon_purgehap_editor2.chrom.sizes

