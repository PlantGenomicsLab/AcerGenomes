#!/bin/bash
#SBATCH --job-name=3dpost
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 9
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=245G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o 3ddna_%j.out
#SBATCH -e 3ddna_%j.err

hostname
echo "\nStart time:"
date

bash /labs/Wegrzyn/local_software/3d-dna/run-asm-pipeline-post-review.sh -r cns_p_ctg_repeats.final.review.assembly /home/CAM/smcevoy/acne/assemblies/falcon/purgehap/cns_p_ctg_repeats.fasta ../../juicer/falcon_purgehap_foption/aligned/merged_nodups.txt

echo "\nEnd time:"
date

