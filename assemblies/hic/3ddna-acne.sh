#!/bin/bash
#SBATCH --job-name=3ddna4
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 25
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=245G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o 3ddna_%j.out
#SBATCH -e 3ddna_%j.err

hostname
echo "\nStart time:"
date

bash /labs/Wegrzyn/local_software/3d-dna/run-asm-pipeline.sh --editor-repeat-coverage 4 /home/CAM/smcevoy/acne/assemblies/falcon/purgehap/cns_p_ctg_repeats.fasta  ../../juicer/falcon_purgehap_foption/aligned/merged_nodups.txt

echo "\nEnd time:"
date

