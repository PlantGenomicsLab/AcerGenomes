#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/3.0.2b
module unload augustus/3.2.3
module unload blast/2.7.1

# cp augustus to home to avoid permission error
export PATH=/home/CAM/smcevoy/augustus/bin:/home/CAM/smcevoy/augustus/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config

input="plastids_rmd.fasta"

/isg/shared/apps/busco/3.0.2b/bin/run_BUSCO.py -i "$input" -l /isg/shared/databases/busco_lineages/embryophyta_odb9/ -o busco_plastids_o -m geno

