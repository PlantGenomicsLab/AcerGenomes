#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o quast_%j.out
#SBATCH -e quast_%j.err

input="cns_p_ctg_repeats.FINAL.fasta"
module load quast
python /isg/shared/apps/quast/5.0.2/quast.py "$input" --large -o quast_postreview_o -t 9
