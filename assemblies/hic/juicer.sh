#!/bin/bash
#SBATCH --job-name=juicer
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH -p general
#SBATCH -q general
###SBATCH --mail-type=END
#SBATCH --mem=50G
###SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o juicer_%j.out
#SBATCH -e juicer_%j.err

module load java-sdk/1.8.0_92
module load bwa/0.7.17

#python /labs/Wegrzyn/local_software/juicer/misc/generate_site_positions.py Sau3AI acne_falcon_purgehap /home/CAM/smcevoy/acne/assemblies/falcon/purgehap/cns_p_ctg_repeats.fasta

#awk 'BEGIN{OFS="\t"}{print $1, $NF}' acne_falcon_purgehap_Sau3AI.txt > acne_falcon_purgehap.chrom.sizes

bash /labs/Wegrzyn/local_software/juicer/SLURM/scripts/juicer.sh -g acne_falcon_purgehap -y acne_falcon_purgehap_Sau3AI.txt -z /home/CAM/smcevoy/acne/assemblies/falcon/purgehap/cns_p_ctg_repeats.fasta -p acne_falcon_purgehap.chrom.sizes

