import re

filehandle = "cns_p_ctg_repeats.FINAL.fasta"

def readFile(filehandle):
  f = open(filehandle, 'r')
  content = f.read()
  seqdata = []
  seqList = re.split(">", content, flags=re.MULTILINE)
  del seqList[0]
  for seq in seqList:
    header, sequence = seq.split("\n",1)
    seqdata.append(sequence.replace("\n", ""))
  return seqdata



seqs = readFile(filehandle)
l = []
for s in seqs:
  l.append(len(s))
l.sort()
print(l)
