#!/bin/bash
#SBATCH --job-name=concatlibs
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o concatlibs_%j.out
#SBATCH -e concatlibs_%j.err

echo `hostname`
cat *R1.fastq > trimmed_all_R1.fastq
cat *R2.fastq > trimmed_all_R2.fastq
cat trimmed_singles_* > trimmed_singles_all.fastq
