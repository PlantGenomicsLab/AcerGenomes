#!/bin/bash
#SBATCH --job-name=sickle
#SBATCH -N 1
#SBATCH -n 4
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o sickle_%j.out
#SBATCH -e sickle_%j.err

sickle pe -f L007_R1.fastq.gz -r L007_R2.fastq.gz -t sanger -o trimmed_L007_R1.fastq -p trimmed_L007_R2.fastq -s trimmed_singles_L007.fastq -q 30 -l 100
sickle pe -f L008_R1.fastq.gz -r L008_R2.fastq.gz -t sanger -o trimmed_L008_R1.fastq -p trimmed_L008_R2.fastq -s trimmed_singles_L008.fastq -q 30 -l 100
