#!/bin/bash
#SBATCH --job-name=purgedups
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o purgedups_%j.out
#SBATCH -e purgedups_%j.err

#module load python/3.6.3

#export PATH="${PATH}:/labs/Wegrzyn/local_software/runner/runner/"

#/labs/Wegrzyn/local_software/purge_dups/scripts/pd_config.py -s shortreads.fastq.fofn -l acne -n config.acne.json ../acne_flye_polar_star_renamed.fasta subreads.fasta.fofn

/labs/Wegrzyn/local_software/purge_dups/scripts/run_purge_dups.py -p bash config.acne.json /labs/Wegrzyn/local_software/purge_dups/bin acne
