#!/bin/bash
#SBATCH --job-name=splitfasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G
#SBATCH -o splitfasta_%j.out
#SBATCH -e splitfasta_%j.err

module load anaconda

filename="curated.fasta"
basedir="/labs/Wegrzyn/AcerGenomes/acsa/genome"
filedir=$basedir/pieces

./splitfasta.py --fasta $filedir/$filename --pieces 100
