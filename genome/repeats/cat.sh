#!/bin/bash
#SBATCH --job-name=cat
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o cat_%j.out
#SBATCH -e cat_%j.err

cat /scratch/smcevoy/repeatmasker_ph/acne_curated/*tbl > curated.fasta.fa.tbl
cat /scratch/smcevoy/repeatmasker_ph/acne_curated/*.out > curated.fasta.fa.out
cat /scratch/smcevoy/repeatmasker_ph/acne_curated/*.out.gff > curated.fasta.fa.out.gff
cat /scratch/smcevoy/repeatmasker_ph/acne_curated/*.masked > curated.fasta.fa.masked
