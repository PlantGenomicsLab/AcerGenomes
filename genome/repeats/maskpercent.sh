#!/bin/bash
#SBATCH --job-name=maskpercent
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o maskpercent_%j.out
#SBATCH -e maskpercent_%j.err


for f in *.tbl;
do
	masktmp=$(sed -n -e 's/bases masked:\s\+\([0-9]*\).*/\1/p' $f)
	echo $masktmp	
	totaltmp=$(sed -n -e 's/total length:\s\+\([0-9]*\).*/\1/p' $f)
	echo $totaltmp
	let "maskbp += masktmp"
	echo "$maskbp"
	let "totalbp += totaltmp"
	echo "$totalbp"
done
echo "Percentage masked"
echo "$maskbp / $totalbp * 100" | bc
