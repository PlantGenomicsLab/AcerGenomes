#!/bin/bash
# Submission script for Xanadu
#SBATCH --mem=360GB
#SBATCH --job-name=repeatModeler
#SBATCH -o repeatModeler-%j.output
#SBATCH -e repeatModeler-%j.error
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=22
#SBATCH --partition=himem
#SBATCH --qos=himem

# Run the program                 
##### NOTE #####
# After running build database function, index files that have been created
# should be automatically copied to your scratch directory

filename="curated.fasta"
basedir="/labs/Wegrzyn/AcerGenomes/acsa"
filedir="$basedir/genome"
database="Acsacurated"

workdir=/scratch/smcevoy/repeatmodeler_ph/acsa_curated/
cp "$filedir/$filename" "$workdir"
cd "$workdir"

module load RepeatModeler/1.0.8
module load rmblastn/2.2.28
module unload perl/5.28.0
module load perl/5.24.0
export PERL5LIB=/UCHC/PublicShare/szaman/perl5/lib/perl5/


BuildDatabase -name "$database" -engine ncbi "$filename"
nice -n 10 RepeatModeler -engine ncbi -pa 22 -database "$database"
rsync -a ./consensi.fa.classified "$filedir/repeats/consensi.fa.classified"

