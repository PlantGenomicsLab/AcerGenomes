#!/bin/bash
# Submission script for Xanadu
#SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --mem=100GB
#SBATCH --job-name=repeatmasker
#SBATCH -o repeatmasker-%j.output
#SBATCH -e repeatmasker-%j.error
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --array=1-100%20

filename=curated.fasta"$SLURM_ARRAY_TASK_ID".fa
basedir=/scratch/smcevoy/repeatmasker_ph/acsa_curated
echo $filename

module unload perl/5.28.0
module load perl/5.24.0
module load RepeatMasker/4.0.6
export PERL5LIB=/UCHC/PublicShare/szaman/perl5/lib/perl5/

if [ -s $basedir/$filename ]
then
  RepeatMasker $basedir/$filename -lib /labs/Wegrzyn/AcerGenomes/acsa/genome/repeatmodeler/repeats/consensi.fa.classified -pa 4 -gff -a -noisy -low -xsmall| tee $basedir/outputfiles/output_acsa.filtered.sm"$SLURM_ARRAY_TASK_ID".fa.txt
fi
