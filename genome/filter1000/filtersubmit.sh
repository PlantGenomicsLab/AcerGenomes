#!/bin/bash
#SBATCH --job-name=filter1000
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH -o filter1000_%j.out
#SBATCH -e filter1000_%j.err

module load biopython/1.70
module load numpy/1.6.2

#filename="curated.fasta.masked"
#filedir="/labs/Wegrzyn/AcerGenomes/acsa/genome"

#./filterLen.py --fasta $filename --path $filedir --length 1000 --out $filename.filtered --pathOut $filedir
grep -c '>' ../curated.fasta.masked.filtered
