#!/bin/bash
#SBATCH --job-name=renameheader
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o renameheader_%j.out
#SBATCH -e renameheader_%j.err

awk '/^>/{print ">acsa_" sprintf("%04d", ++i); next}{ print }' < curated.fasta.masked.filtered > curated.fasta.masked.filtered.headers

