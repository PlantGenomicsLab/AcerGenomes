#!/bin/bash
#SBATCH --job-name=jf21
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=475G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o jf21_%j.out
#SBATCH -e jf21_%j.err

module load jellyfish/2.2.6
jellyfish count -t 30 -C -m 21 -s 100G -o 21mer_out --min-qual-char=? /home/CAM/smcevoy/acers/illumina/acsa/sickle/trimmed_all_R1.fastq /home/CAM/smcevoy/acers/illumina/acsa/sickle/trimmed_all_R2.fastq
