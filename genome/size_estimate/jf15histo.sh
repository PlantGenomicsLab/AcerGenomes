#!/bin/bash
#SBATCH --job-name=jf15-h
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o jf15-h_%j.out
#SBATCH -e jf15-h_%j.err

module load jellyfish/2.2.6

jellyfish histo -o 15mer_out.histo 15mer_out
