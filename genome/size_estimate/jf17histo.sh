#!/bin/bash
#SBATCH --job-name=jf17-h
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o jf17-h_%j.out
#SBATCH -e jf17-h_%j.err

module load jellyfish/2.2.6

jellyfish histo -o 17mer_out.histo 17mer_out
